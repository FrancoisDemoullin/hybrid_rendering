#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <headers/filesystem.h>
#include <headers/shader_m.h>
#include <headers/camera.h>
#include <headers/model.h>

#include <headers/simple_model.h>
#include <headers/light_manager.h>

#include <iostream>

void framebuffer_size_callback(GLFWwindow *window, int width, int height);
void mouse_callback(GLFWwindow *window, double xpos, double ypos);
void scroll_callback(GLFWwindow *window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);
void save_screenshot(string filename, int w, int h);

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

const unsigned int SHADOW_WIDTH = 1024;
const unsigned int SHADOW_HEIGHT = 1024;

const float near_plane = 1.0f, far_plane = 7.5f;

// camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

// timing
float deltaTime = 0.0f;
float lastFrame = 0.0f;

void renderDepthMap(const Shader &depthMapShader, LightManager &lightManager,
                    unsigned int depthMapFBO, vector<SimpleModel> &modelVec)
{

  SimpleModel teapot = modelVec[0];
  SimpleModel cornell = modelVec[1];
  SimpleModel light = modelVec[2];
  SimpleModel cube = modelVec[3];

  glm::vec3 lightPos = lightManager.getLights()[0].position;

  glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // 1. render depth of scene to texture (from light's perspective)
  // --------------------------------------------------------------
  glm::mat4 lightProjection, lightView;
  glm::mat4 lightSpaceMatrix;
  float near_plane = 0.2f, far_plane = 100.0f; // 7.5
  float aspect = 0.2f;

  lightProjection = glm::perspective(glm::radians(45.0f),
  (GLfloat)SHADOW_WIDTH / (GLfloat)SHADOW_HEIGHT, near_plane, far_plane); //
  // note that if you use a perspective projection matrix you'll have to change
  // the light position as the current light position isn't enough to reflect the
  // whole scene
  lightProjection =
      glm::ortho(-aspect, aspect, -aspect, aspect, near_plane, far_plane);
  lightView = glm::lookAt(lightPos, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
  lightSpaceMatrix = lightProjection * lightView;
  // render scene from light's point of view
  depthMapShader.use();
  depthMapShader.setMat4("lightSpaceMatrix", lightSpaceMatrix);

  glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
  glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
  glClear(GL_DEPTH_BUFFER_BIT);

  glm::mat4 model_mat;
  depthMapShader.setMat4("model", model_mat);

  // render scene
  for (auto model : modelVec)
  {
    //model.Draw(depthMapShader);
  }

  model_mat = glm::mat4();
  model_mat = glm::translate(model_mat, glm::vec3(0.2f, 0.1f, 0.1f));
  model_mat = glm::scale(model_mat, glm::vec3(0.1f, 0.15f, 0.1f));
  // model_mat = glm::scale(model_mat, glm::vec3(8.f));
  depthMapShader.setMat4("model", model_mat);
  cube.Draw(depthMapShader);

  model_mat = glm::mat4();
  model_mat = glm::translate(model_mat, glm::vec3(0.0f, 0.1f, 0.0f));
  model_mat = glm::scale(model_mat, glm::vec3(0.005f, 0.005f, 0.005f));
  // model_mat = glm::scale(model_mat, glm::vec3(8.f));
  depthMapShader.setMat4("model", model_mat);
  teapot.Draw(depthMapShader);

  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  // reset viewport
  glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void drawScene(vector<Shader> &shaderVec, vector<SimpleModel> &modelVec,
               LightManager &lightManager)
{

  // parse the vector arguments
  Shader teapotShader = shaderVec[0];
  Shader cornellShader = shaderVec[1];
  Shader lightShader = shaderVec[2];

  SimpleModel teapot = modelVec[0];
  SimpleModel cornell = modelVec[1];
  SimpleModel light = modelVec[2];
  SimpleModel cube = modelVec[3];

  // draw the scene
  cornellShader.use();

  glm::mat4 projection =
      glm::perspective(glm::radians(camera.Zoom),
                       (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
  glm::mat4 view = camera.GetViewMatrix();
  cornellShader.setMat4("projection", projection);
  cornellShader.setMat4("view", view);

  glm::mat4 model;
  model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
  model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
  cornellShader.setMat4("model", model);

  cornell.DrawLit(cornellShader, lightManager, camera.Position);

  lightShader.use();
  lightShader.setMat4("projection", projection);
  lightShader.setMat4("view", view);
  lightShader.setMat4("model", model);
  light.Draw(lightShader);

  teapotShader.use();
  teapotShader.setMat4("projection", projection);
  teapotShader.setMat4("view", view);

  model = glm::mat4();
  model = glm::translate(model, glm::vec3(0.0f, 0.1f, 0.0f));
  model = glm::scale(model, glm::vec3(0.005f, 0.005f, 0.005f));
  teapotShader.setMat4("model", model);

  teapot.DrawLit(teapotShader, lightManager, camera.Position);

  // first cube
  cornellShader.use();
  model = glm::mat4();
  model = glm::translate(model, glm::vec3(0.2f, 0.1f, 0.1f));
  model = glm::scale(model, glm::vec3(0.1f, 0.15f, 0.1f));
  // model = glm::rotate(model, 0.5f, glm::vec3(0.f, 1.f, 0.f));
  cornellShader.setMat4("model", model);
  cube.DrawLit(cornellShader, lightManager, camera.Position);
}

// renderQuad() renders a 1x1 XY quad in NDC
// -----------------------------------------
unsigned int quadVAO = 0;
unsigned int quadVBO;
void renderQuad()
{
  if (quadVAO == 0)
  {
    float quadVertices[] = {
        // positions        // texture Coords
        -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
        1.0f,  1.0f, 0.0f, 1.0f, 1.0f, 1.0f,  -1.0f, 0.0f, 1.0f, 0.0f,
    };
    // setup plane VAO
    glGenVertexArrays(1, &quadVAO);
    glGenBuffers(1, &quadVBO);
    glBindVertexArray(quadVAO);
    glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices,
                 GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
                          (void *)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
                          (void *)(3 * sizeof(float)));
  }
  glBindVertexArray(quadVAO);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  glBindVertexArray(0);
}

int main()
{
  // glfw: initialize and configure
  // ------------------------------
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
  glfwWindowHint(
      GLFW_OPENGL_FORWARD_COMPAT,
      GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

  // glfw window creation
  // --------------------
  GLFWwindow *window =
      glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "hybrid_rendering", NULL, NULL);
  if (window == NULL)
  {
    std::cout << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
    return -1;
  }
  glfwMakeContextCurrent(window);
  glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
  glfwSetCursorPosCallback(window, mouse_callback);
  glfwSetScrollCallback(window, scroll_callback);

  // tell GLFW to capture our mouse
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // glad: load all OpenGL function pointers
  // ---------------------------------------
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
  {
    std::cout << "Failed to initialize GLAD" << std::endl;
    return -1;
  }

  // configure global opengl state
  // -----------------------------
  glEnable(GL_DEPTH_TEST);

  // build and compile shaders
  // -------------------------
  Shader teapotShader("../src/teapot.vs", "../src/teapot.fs");
  Shader cornellShader("../src/cornell.vs", "../src/cornell.fs");
  Shader lightShader("../src/light.vs", "../src/light.fs");
  vector<Shader> shaderVec = {teapotShader, cornellShader, lightShader};

  Shader depthMapShader("../src/depth_shadow_map.vs",
                        "../src/depth_shadow_map.fs");
  Shader textureDebugShader("../src/texture_debug.vs",
                            "../src/texture_debug.fs");

  // initialize the light manager
  LightManager lightManager;

  // load models
  // -----------
  SimpleModel teapot("../resources/Teapot.obj", lightManager);
  SimpleModel cornell("../resources/cornell.obj", lightManager);
  SimpleModel light("../resources/light.obj", lightManager);
  SimpleModel cube("../resources/cube.obj", lightManager);

  vector<SimpleModel> modelVec = {teapot, cornell, light, cube};

  // draw in wireframe
  // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  // configure depth map FBO
  // -----------------------
  unsigned int depthMapFBO;
  glGenFramebuffers(1, &depthMapFBO);
  // create depth texture
  unsigned int depthMap;
  glGenTextures(1, &depthMap);
  glBindTexture(GL_TEXTURE_2D, depthMap);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH,
               SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
  float borderColor[] = {1.0, 1.0, 1.0, 1.0};
  glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
  // attach depth texture as FBO's depth buffer
  glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
                         depthMap, 0);
  glDrawBuffer(GL_NONE);
  glReadBuffer(GL_NONE);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  // render loop
  // -----------
  while (!glfwWindowShouldClose(window))
  {
    // per-frame time logic
    // --------------------
    float currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // input
    // -----
    processInput(window);

    // shadow pass
    renderDepthMap(depthMapShader, lightManager, depthMapFBO, modelVec);

    // render
    // ------
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, depthMap);

    // drawScene(shaderVec, modelVec, lightManager);

    textureDebugShader.use();
    textureDebugShader.setFloat("near_plane", near_plane);
    textureDebugShader.setFloat("far_plane", far_plane);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, depthMap);
    renderQuad();

    // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved
    // etc.)
    // -------------------------------------------------------------------------------
    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  // glfw: terminate, clearing all previously allocated GLFW resources.
  // ------------------------------------------------------------------
  glfwTerminate();
  return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this
// frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
  if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    glfwSetWindowShouldClose(window, true);

  if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    camera.ProcessKeyboard(FORWARD, deltaTime);
  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    camera.ProcessKeyboard(BACKWARD, deltaTime);
  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    camera.ProcessKeyboard(LEFT, deltaTime);
  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    camera.ProcessKeyboard(RIGHT, deltaTime);

  if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
  {
    cout << "saving screenshot" << endl;
    save_screenshot("/home/francois/CLionProjects/hybrid_renderer_0/resources/"
                    "current_image.tga",
                    SCR_WIDTH, SCR_HEIGHT);
  }
}

// glfw: whenever the window size changed (by OS or user resize) this callback
// function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
  // make sure the viewport matches the new window dimensions; note that width
  // and
  // height will be significantly larger than specified on retina displays.
  glViewport(0, 0, width, height);
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow *window, double xpos, double ypos)
{
  if (firstMouse)
  {
    lastX = xpos;
    lastY = ypos;
    firstMouse = false;
  }

  float xoffset = xpos - lastX;
  float yoffset =
      lastY - ypos; // reversed since y-coordinates go from bottom to top

  lastX = xpos;
  lastY = ypos;

  camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow *window, double xoffset, double yoffset)
{
  camera.ProcessMouseScroll(yoffset);
}

// screenshot function
// ----------------------------------------------------------------------
void save_screenshot(string filename, int w, int h)
{
  // This prevents the images getting padded
  // when the width multiplied by 3 is not a multiple of 4
  glPixelStorei(GL_PACK_ALIGNMENT, 1);

  int nSize = w * h * 3;
  // First let's create our buffer, 3 channels per Pixel
  char *dataBuffer = (char *)malloc(nSize * sizeof(char));

  if (!dataBuffer)
    assert(0);

  // Let's fetch them from the backbuffer
  // We request the pixels in GL_BGR format, thanks to Berzeger for the tip
  glReadPixels((GLint)0, (GLint)0, (GLint)w, (GLint)h, GL_BGR, GL_UNSIGNED_BYTE,
               dataBuffer);

  // Now the file creation
  FILE *filePtr = fopen(filename.c_str(), "wb");
  if (!filePtr)
    assert(0);

  unsigned char TGAheader[12] = {0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  unsigned char header[6] = {(unsigned char)(w % 256),
                             (unsigned char)(w / 256),
                             (unsigned char)(h % 256),
                             (unsigned char)(h / 256),
                             24,
                             0};
  // We write the headers
  fwrite(TGAheader, sizeof(unsigned char), 12, filePtr);
  fwrite(header, sizeof(unsigned char), 6, filePtr);
  // And finally our image data
  fwrite(dataBuffer, sizeof(GLubyte), nSize, filePtr);
  fclose(filePtr);

  free(dataBuffer);
}
