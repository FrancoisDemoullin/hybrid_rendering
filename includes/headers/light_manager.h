//
// Created by francois on 9/9/18.
//

#ifndef HYBRID_RENDERER_0_LIGHT_H
#define HYBRID_RENDERER_0_LIGHT_H

#include <glad/glad.h>

#include <glm/glm.hpp>

#include <vector>

struct my_light {
    glm::vec3 position;

    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
};

class LightManager {

public:

    LightManager() {
      // initialize lights vector to empty
      lights = std::vector<my_light>();
    }

    std::vector<my_light> getLights() {
      return lights;
    }

    void addLight(my_light add_me) {
      lights.push_back(add_me);
    }

private:
    std::vector<my_light> lights;
};

#endif //HYBRID_RENDERER_0_LIGHT_H
