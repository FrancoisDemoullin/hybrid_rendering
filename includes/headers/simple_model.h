//
// Created by francois on 07/09/18.
//

#ifndef HYBRID_RENDERER_0_SIMPLE_MODEL_H
#define HYBRID_RENDERER_0_SIMPLE_MODEL_H

#include <glad/glad.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stb_image.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <headers/shader.h>
#include <headers/simple_mesh.h>
#include <headers/light_manager.h>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
#include <stdio.h>

using namespace std;

/*
 * This class follows the model class but does not load textures
 * All attributes are passed to the shaders using the material struct
 * */
class SimpleModel
{
public:
    /*  Model Data */
    vector<my_material> materials;
    vector<SimpleMesh> meshes;
    bool gammaCorrection;

    /*  Functions   */
    // constructor, expects a filepath to a 3D model.
    SimpleModel(string const &path, LightManager &lightManager, bool gamma = false) : gammaCorrection(gamma)
    {
      loadModel(path, lightManager);
    }

    // draws the model, and thus all its meshes
    void Draw(Shader shader)
    {
      for(unsigned int i = 0; i < meshes.size(); i++)
        meshes[i].Draw(shader);
    }

    void DrawLit(Shader shader, LightManager lightManager, glm::vec3 viewPos)
    {
      for(unsigned int i = 0; i < meshes.size(); i++)
        meshes[i].DrawLit(shader, lightManager, viewPos);
    }

private:
    /*  Functions   */
    // loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
    void loadModel(string const &path, LightManager &lightManager)
    {
      // read file via ASSIMP
      Assimp::Importer importer;
      const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
      // check for errors
      if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
      {
        cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << endl;
        return;
      }

      // process ASSIMP's root node recursively
      processNode(scene->mRootNode, scene, lightManager);
    }

    // processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
    void processNode(aiNode *node, const aiScene *scene, LightManager &lightManager)
    {
      // process each mesh located at the current node
      for(unsigned int i = 0; i < node->mNumMeshes; i++)
      {
        // the node object only contains indices to index the actual objects in the scene.
        // the scene contains all the data, node is just to keep stuff organized (like relations between nodes).
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        meshes.push_back(processMesh(mesh, scene, lightManager));
      }
      // after we've processed all of the meshes (if any) we then recursively process each of the children nodes
      for(unsigned int i = 0; i < node->mNumChildren; i++)
      {
        processNode(node->mChildren[i], scene, lightManager);
      }

    }

    SimpleMesh processMesh(aiMesh *mesh, const aiScene *scene, LightManager &lightManager)
    {
      // data to fill
      vector<Vertex> vertices;
      vector<unsigned int> indices;
      vector<Texture> textures;

      // Walk through each of the mesh's vertices
      for(unsigned int i = 0; i < mesh->mNumVertices; i++)
      {
        Vertex vertex;
        glm::vec3 vector; // we declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
        // positions
        vector.x = mesh->mVertices[i].x;
        vector.y = mesh->mVertices[i].y;
        vector.z = mesh->mVertices[i].z;
        vertex.Position = vector;
        // normals
        if (mesh->mNormals) {
          vector.x = mesh->mNormals[i].x;
          vector.y = mesh->mNormals[i].y;
          vector.z = mesh->mNormals[i].z;
          vertex.Normal = vector;
        } else {
          vertex.Normal = glm::vec3(0.0f, 0.0f, 0.0f);
        }

        // texture coordinates
        if(mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
        {
          glm::vec2 vec;
          // a vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
          // use models where a vertex can have multiple texture coordinates so we always take the first set (0).
          vec.x = mesh->mTextureCoords[0][i].x;
          vec.y = mesh->mTextureCoords[0][i].y;
          vertex.TexCoords = vec;
        }
        else
        {
          vertex.TexCoords = glm::vec2(0.0f, 0.0f);
        }

        // tangent
        if (mesh->mTangents) {
          vector.x = mesh->mTangents[i].x;
          vector.y = mesh->mTangents[i].y;
          vector.z = mesh->mTangents[i].z;
          vertex.Tangent = vector;
        } else {
          // no tangent
          vertex.Tangent = glm::vec3(0.0f, 0.0f, 0.0f);
        }

        // bitangent
        if (mesh->mBitangents)
        {
          vector.x = mesh->mBitangents[i].x;
          vector.y = mesh->mBitangents[i].y;
          vector.z = mesh->mBitangents[i].z;
          vertex.Bitangent = vector;
        } else {
          // no bitangent
          vertex.Bitangent = glm::vec3(0.0f, 0.0f, 0.0f);
        }

        vertices.push_back(vertex);
      }
      // now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
      for(unsigned int i = 0; i < mesh->mNumFaces; i++)
      {
        aiFace face = mesh->mFaces[i];
        // retrieve all indices of the face and store them in the indices vector
        for(unsigned int j = 0; j < face.mNumIndices; j++)
          indices.push_back(face.mIndices[j]);
      }
      // process materials
      aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

      // populate the material struct

      aiColor3D aiColor;
      // ambient
      material->Get(AI_MATKEY_COLOR_AMBIENT, aiColor);
      glm::vec3 ambient(aiColor.r, aiColor.g, aiColor.b);

      // diffuse
      material->Get(AI_MATKEY_COLOR_DIFFUSE, aiColor);
      glm::vec3 diffuse(aiColor.r, aiColor.g, aiColor.b);

      // specular
      material->Get(AI_MATKEY_COLOR_SPECULAR, aiColor);
      glm::vec3 specular(aiColor.r, aiColor.g, aiColor.b);

      float shininess;
      material->Get(AI_MATKEY_SHININESS, shininess);

      // init the struct
      my_material mat = {ambient, diffuse, specular, shininess};

      // check whether current object is a light source
      // this is a hack! We simply scan the name of the mtl for the word "my_light"
      // this should be done by checking the emissive component!
      aiString name;
      material->Get(AI_MATKEY_NAME, name);
      if (strstr(name.data, "my_light")) {

        // reduce the light to a point light
        // we just average all the vertices of the light
        // this is a hack!
        glm::vec3 light_position;
        for (std::vector<Vertex>::iterator it = vertices.begin() ; it != vertices.end(); ++it) {
          light_position += (*it).Position;
        }
        light_position /= vertices.size();

        my_light light = {light_position, ambient, diffuse, specular};

        // add the light to the lightmanager
        lightManager.addLight(light);
      }

      // return a mesh object created from the extracted mesh data
      return SimpleMesh(vertices, indices, mat);
    }
};

#endif //HYBRID_RENDERER_0_SIMPLE_MODEL_H
