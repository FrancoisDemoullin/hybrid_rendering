//
// Created by francois on 07/09/18.
//

#ifndef HYBRID_RENDERER_0_SIMPLE_MESH_H
#define HYBRID_RENDERER_0_SIMPLE_MESH_H

#include <glad/glad.h> // holds all OpenGL type declarations

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <headers/shader.h>
#include <headers/mesh.h>
#include <headers/light_manager.h>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
using namespace std;


struct my_material {
    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
    float shininess;
    bool is_light;
};

class SimpleMesh {
public:
    /*  Mesh Data  */
    vector<Vertex> vertices;
    vector<unsigned int> indices;
    my_material material;
    unsigned int VAO;

    /*  Functions  */
    // constructor
    SimpleMesh(vector<Vertex> vertices, vector<unsigned int> indices, my_material &material)
    {
      this->vertices = vertices;
      this->indices = indices;
      this->material = material;

      // now that we have all the required data, set the vertex buffers and its attribute pointers.
      setupMesh();
    }

    // render the mesh
    void Draw(Shader shader)
    {
      // bind material properties
      shader.setVec3("material.ambient", material.ambient);
      shader.setVec3("material.diffuse", material.diffuse);
      shader.setVec3("material.specular", material.specular);
      shader.setFloat("material.shininess", material.shininess);

      // draw mesh
      glBindVertexArray(VAO);
      glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
      glBindVertexArray(0);

      // always good practice to set everything back to defaults once configured.
      glActiveTexture(GL_TEXTURE0);
    }

    void DrawLit(Shader shader, LightManager lightManager, glm::vec3 viewPos)
    {
      // bind material properties
      shader.setVec3("material.ambient", material.ambient);
      shader.setVec3("material.diffuse", material.diffuse);
      shader.setVec3("material.specular", material.specular);
      shader.setFloat("material.shininess", material.shininess);

      // bind light properties

      // TODO: make this accept multipl lights
      my_light light = lightManager.getLights()[0];

      shader.setVec3("light.position", light.position);
      shader.setVec3("light.ambient", light.ambient);
      shader.setVec3("light.diffuse", light.diffuse);
      shader.setVec3("light.specular", light.specular);

      shader.setVec3("viewPos", viewPos);


      // draw mesh
      glBindVertexArray(VAO);
      glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
      glBindVertexArray(0);

      // always good practice to set everything back to defaults once configured.
      glActiveTexture(GL_TEXTURE0);
    }

private:
    /*  Render data  */
    unsigned int VBO, EBO;

    /*  Functions    */
    // initializes all the buffer objects/arrays
    void setupMesh()
    {
      // create buffers/arrays
      glGenVertexArrays(1, &VAO);
      glGenBuffers(1, &VBO);
      glGenBuffers(1, &EBO);

      glBindVertexArray(VAO);
      // load data into vertex buffers
      glBindBuffer(GL_ARRAY_BUFFER, VBO);
      // A great thing about structs is that their memory layout is sequential for all its items.
      // The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
      // again translates to 3/2 floats which translates to a byte array.
      glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

      // set the vertex attribute pointers
      // vertex Positions
      glEnableVertexAttribArray(0);
      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
      // vertex normals
      glEnableVertexAttribArray(1);
      glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));
      // vertex texture coords
      glEnableVertexAttribArray(2);
      glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));
      // vertex tangent
      glEnableVertexAttribArray(3);
      glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Tangent));
      // vertex bitangent
      glEnableVertexAttribArray(4);
      glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Bitangent));

      glBindVertexArray(0);
    }
};

#endif //HYBRID_RENDERER_0_SIMPLE_MESH_H
